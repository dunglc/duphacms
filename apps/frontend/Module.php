<?php
namespace Dupha\Frontend;
use Phalcon\Dispatcher,
    Phalcon\Mvc\View,
    Phalcon\Mvc\Dispatcher as MvcDispatcher,
    Phalcon\Events\Manager as EventsManager,
    Phalcon\Mvc\Dispatcher\Exception as DispatchException;
class Module
{
    public function registerAutoloaders(){
        $loader = new \Phalcon\Loader();
        $loader->registerNamespaces(array(
            'Dupha\Frontend\Controllers' => APP_DIR.'/frontend/controllers/',
            'Dupha\Backend\Models'      => APP_DIR.'/frontend/models/',
            'Dupha\Frontend\Forms'      => APP_DIR.'/frontend/forms/',
        ));
        $loader->register();
    }
    public function registerServices($di){

        $di->set('dispatcher', function () {
            //Create/Get an EventManager
            $eventsManager = new \Phalcon\Events\Manager();
            //Attach a listener
            $eventsManager->attach("dispatch", function ($event, $dispatcher, $exception) {
                //controller or action doesn't exist
                if ($event->getType() == 'beforeException') {
                    switch ($exception->getCode()) {
                        case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                        case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                            $dispatcher->forward(array(
                                'module'=>'frontend',
                                'controller' => 'index',
                                'action' => 'route404'
                            ));

                            return false;
                    }
                }
            });
            $dispatcher = new \Phalcon\Mvc\Dispatcher();
            $dispatcher->setDefaultNamespace("Dupha\Frontend\Controllers");
            return $dispatcher;
        });

        // $di->set('view', function() {
        //     $view = new View();
        //     $view->setViewsDir(APP_DIR.'/frontend/views/');
        //     return $view;
        // });
$config = include APP_DIR . '/config/config.php';
        $di->set('view', function() {
            $view = new \Phalcon\Mvc\View();
            $view->setViewsDir('../apps/frontend/views/');
            $view->registerEngines(array(
                ".volt" => function($view, $di) {
                        $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
                        $volt->setOptions(array(
                            "compiledPath" => APP_DIR."/cache/volt/",
                            "compiledExtension" => ".compiled",
                            'compiledSeparator' => '_',
                            'compileAlways' => true,
                        ));

                        $volt->getCompiler()->addFunction('_', function ($resolvedArgs, $exprArgs) use ($di) {
                            return sprintf('$this->translate->query(\'%s\')', $exprArgs[0]['expr']['value']);
                        });
                        $volt->getCompiler()->addFunction('numberFormat','number_format');
                        $volt->getCompiler()->addFunction('strrepeat', 'str_repeat');
                        $volt->getCompiler()->addFunction('strpos', 'strpos');
                        $volt->getCompiler()->addFunction('unserialize', 'unserialize');
                        $volt->getCompiler()->addFunction('array_search','array_search');
                        $volt->getCompiler()->addFunction('strlen','strlen');
                        $volt->getCompiler()->addFunction('count','count');
                        $volt->getCompiler()->addFunction('unserialize','unserialize');
                         $volt->getCompiler()->addFunction('substr','substr');
                        return $volt;
                    }
            ));
            return $view;
        });

    
    }

}