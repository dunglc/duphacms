<?php

error_reporting(E_ALL);

try {

    /**
     * Define some useful constants
     */
    define('BASE_DIR', dirname(__DIR__));
    define('APP_DIR', BASE_DIR . '/apps');
    define('APPLICATION_PATH', realpath(dirname(__DIR__)));
    
	/**
	 * Read the configuration
	 */
	$config = include APP_DIR . '/config/config.php';

	/**
	 * Read auto-loader
	 */
	include APP_DIR . '/config/loader.php';

	/**
	 * Read services
	 */
	include APP_DIR . '/config/services.php';

	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application($di);
	$application->registerModules(array(
		'frontend' => array(
			'className' => 'Dupha\Frontend\Module',
			'path' => APP_DIR.'/frontend/Module.php'
		),
		'backend' => array(
			'className' => 'Dupha\Backend\Module',
			'path' => APP_DIR.'/backend/Module.php'
		)
	));
	echo $application->handle()->getContent();

} catch (Exception $e) {
	echo $e->getMessage(), '<br>';
	echo nl2br(htmlentities($e->getTraceAsString()));
}
